=============
Release notes
=============

.. toctree::
   :maxdepth: 2

   releases/next
   releases/v2016.1.0
